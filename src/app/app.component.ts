import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventService } from './services/api/event.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SocketService } from './services/socket/socket.service';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public subDomain: string;
  public isBoard: boolean = false;
  public isIe = false;

  constructor(
    private router: Router,
    private eventService: EventService,
    private deviceService: DeviceDetectorService,
    private socketService: SocketService
  ) {
    this.router.events
      .pipe(filter((e): e is NavigationEnd => e instanceof NavigationEnd))
      .subscribe((event) => {
        this.isBoard = (event.url === '/board' || event.url === '/board-regist');
        // * 페이지 이동시 열려있는 모달 모두 닫기
        $('.modal').modal('hide');
      });

    // 소켓 초기화
    this.socketService.init();
  }

  ngOnInit(): void {
    const device = this.deviceService.getDeviceInfo();
    if (device.browser === 'IE') {
      this.isIe = true;
      this.router.navigate(['/not-ie']);
    }

    this.getDomain(); // url에서 행사명 추출
    this.setEvent(); // 행사명으로 Event 찾기
  }

  getDomain(): boolean {
    const domain = window.location.hostname;

    // 로컬 환경에서 행사명 지정
    if (domain.includes('localhost') || domain.includes('192')) {
      // this.subDomain = 'icdm2020';
      this.subDomain = '테스트 행사 1';
      return true;
    }

    // URL에서 행사명 가져오기
    if (domain.indexOf('.') < 0 ||
      domain.split('.')[0] === 'example' || domain.split('.')[0] === 'lvh' || domain.split('.')[0] === 'www') {
      this.subDomain = '';
    } else {
      this.subDomain = domain.split('.')[0];
    }
  }

  setEvent(): any {
    this.eventService.findOne().subscribe(res => {
      // console.log('GET EventInfo', res);
    });
  }

}
