import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, HostListener } from '@angular/core';
import { SpeakerService } from 'src/app/services/api/speaker.service';
import { BoothService } from 'src/app/services/api/booth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public speakers: Array<any> = [];
  public booths: Array<any> = []; // 카테고리[부스] 목록
  public selectedBooth: any;
  public mobile: boolean;
  public excerptUrl = ''; // TODO ICDM 초록집 URL 기입

  public attachments = []; // 부스 첨부파일

  constructor(
    private speakerService: SpeakerService,
    private boothService: BoothService,
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth < 768 !== this.mobile) {
      this.loadSpeakers();
    }
  }

  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)]
      })
    );
  }

  ngOnInit(): void {
    this.loadSpeakers();
    this.loadBooths();
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers() {
    let limit = 4;
    this.mobile = window.innerWidth < 768;
    if (this.mobile) limit = 6;
    this.speakerService.find().subscribe(res => {
      this.speakers = this.division(res, limit);
    });
  }

  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      // console.log('GET Booths', res);

      const categories =
        _.chain(res)
          .groupBy(booth => {
            return booth.category ? JSON.stringify(booth.category) : '{}';
          })
          .map((booth, category) => {
            category = JSON.parse(category);
            category.booths = booth;

            // console.log('category= ', category)
            return category;
          }).sortBy(category => {
            return category.seq;
          })
          .value();

      this.booths = categories[0].booths;

    });
  }

  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  // 부스 상세보기
  getDetail = (selectedBooth) => {
    this.boothService.findOne(selectedBooth.id).subscribe(res => {
      this.selectedBooth = res;
      this.setAttachments();
      this.setDesc();
    });
  }

}
