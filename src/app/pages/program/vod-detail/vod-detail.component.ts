import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { VodService } from 'src/app/services/api/vod.service';
import { NgImageSliderComponent } from 'ng-image-slider';
import { BannerService } from 'src/app/services/api/banner.service';

@Component({
  selector: 'app-vod-detail',
  templateUrl: './vod-detail.component.html',
  styleUrls: ['./vod-detail.component.scss']
})
export class VodDetailComponent implements OnInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;

  public vod: any;
  public banners = [];

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    public vodService: VodService,
    private bannerService: BannerService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      if (params.vodId) {
        this.vodService.findOne(params.vodId).subscribe(vod => {
          this.vod = vod.contents;
        });

        this.getBanners();
      }
    });
  }

  // 리스트로 돌아가기
  goBack(): void {
    this.location.back();
  }

  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      res.vod.forEach(item => {
        const data = {
          link: item.link,
          thumbImage: item.photoUrl,
          alt: item.title,
        };
        this.banners.push(data);
      });
      // console.log(this.banners);
    });
  }

  slidePrev(target): void {
    this[target].prev();
  }
  slideNext(target): void {
    this[target].next();
  }

  imageClick(index): void {
    if (this.banners[index] && this.banners[index].link) {
      window.open(this.banners[index].link);
    }
  }

}
