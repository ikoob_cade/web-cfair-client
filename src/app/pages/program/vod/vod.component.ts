import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { VodService } from 'src/app/services/api/vod.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vod',
  templateUrl: './vod.component.html',
  styleUrls: ['./vod.component.scss']
})
export class VodComponent implements OnInit {

  public categories: Array<any>;

  constructor(
    private vodService: VodService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.loadVods();
  }

  loadVods = () => {
    this.vodService.find().subscribe(res => {
      // console.log('GET Vods', res);
      this.categories = this.sortByCategory(res);
      // console.log(this.categories);
      this.categories.forEach((category) => {
        let groups = {};
        category.vods.forEach((vod) => {
          if (vod.group) {
            if (!groups[vod.group]) {
              groups[vod.group] = [vod];
            } else {
              groups[vod.group].push(vod)
            }
          }
        });
        let groupList = [];
        for (let i in groups) {
          groupList.push(groups[i]);
        }
        if (groupList.length > 0) {
          category.groups = groupList;
          // console.log(groupList);
        }
      });
    });
  }


  sortByCategory = (posters) => {
    return _.chain(posters)
      .groupBy(poster => {
        return poster.category ? JSON.stringify(poster.category) : '{}';
      })
      .map((poster, category) => {
        category = JSON.parse(category);
        category.vods = poster;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }


  goDetail(vod): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }
}
