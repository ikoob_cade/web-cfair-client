import * as _ from 'lodash';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { _ParseAST } from '@angular/compiler';
import { BoothService } from 'src/app/services/api/booth.service';
import { SponsorService } from 'src/app/services/api/sponsor.service';

declare var $: any;
@Component({
  selector: 'app-e-booth',
  templateUrl: './e-booth.component.html',
  styleUrls: ['./e-booth.component.scss']
})
export class EBoothComponent implements OnInit {
  @ViewChild('winAlertBtn') winAlertBtn: ElementRef;

  public sponsors: Array<any> = []; // 스폰서 목록
  public categories: Array<any> = []; // 카테고리[부스] 목록
  public selectedBooth: any;
  public attachments = [];

  public random = 0;

  constructor(
    private boothService: BoothService,
    private sponsorService: SponsorService,
  ) { }

  ngOnInit(): void {
    // this.attachments = this.setAttachments();
    $('#e-boothModal').on('hidden.bs.modal', () => {
      document.getElementById('boothModalDesc').innerHTML = null;
    });
    this.loadBooths();
    this.loadSponsors();
  }

  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  /**
   * 부스 목록 조회
   */
  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      // console.log('GET Booths', res);

      this.categories =
        _.chain(res)
          .groupBy(booth => {
            return booth.category ? JSON.stringify(booth.category) : '{}';
          })
          .map((booth, category) => {
            category = JSON.parse(category);
            category.booths = booth;
            return category;
          }).sortBy(category => {
            return category.seq;
          })
          .value();
    });
  }

  /**
   * 스폰서 목록 조회
   */
  loadSponsors = () => {
    this.sponsorService.find().subscribe(res => {
      // console.log('GET Sponsors', res);
      this.sponsors = res;
    });
  }

  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  getDetail = (selectedBooth) => {
    const user = JSON.parse(localStorage.getItem('cfair'));
    let memberId;

    if (user && user.isLog) { // 국내 연자에게만 스탬프 투어 기록한다.
      memberId = user.id;
    }

    this.boothService.findOne(selectedBooth.id, memberId).subscribe(res => {
      this.selectedBooth = res;

      this.random = Math.floor(Math.random() * 10);

      if (res.tourSuccess) {
        this.winAlertBtn.nativeElement.click();
      }

      this.setAttachments();
      this.setDesc();
    });
  }

}
