import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostersDetailComponent } from './posters-detail.component';

describe('PostersDetailComponent', () => {
  let component: PostersDetailComponent;
  let fixture: ComponentFixture<PostersDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
