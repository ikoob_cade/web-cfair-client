import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;

  private user: any;

  public member = {
    email: '',
    password: '',
  };

  constructor(
    public router: Router,
    public auth: AuthService) { }

  ngOnInit(): void {
  }

  login(): void {
    if (this.loginValidator()) {
      this.auth.login(this.member).subscribe(res => {
        if (res.token) {
          this.user = res;
          this.goToMain();
        }
      }, error => {
        if (error.status === 401) {
          alert('E-mail 또는 비밀번호가 일치하지 않습니다.');
        }
      });
    } else {
      alert('값을 모두 입력해 주세요.');
    }
  }

  loginValidator(): boolean {
    if (!this.member.email || !this.member.password) {
      return false;
    }
    return true;
  }

  /**
   * 로그인 정보를 스토리지 저장 및 메인으로 이동.
   */
  goToMain(): void {
    localStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }
}
