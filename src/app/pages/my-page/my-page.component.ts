import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MemberService } from 'src/app/services/api/member.service';
import { SocketService } from 'src/app/services/socket/socket.service';

declare var $: any;
enum Change_Password_Message {
  FILL_ALL = '모든 항목을 입력해 주세요.',
  FAILED_NEW_CONFIRM = '새 비밀번호 확인이 잘못 입력되었습니다.\n다시 확인해 주세요.',
  SUCCESS = '비밀번호가 변경되었습니다.'
}

@Component({
  selector: 'app-my-page',
  templateUrl: './my-page.component.html',
  styleUrls: ['./my-page.component.scss']
})
export class MyPageComponent implements OnInit {
  public user: any;
  public histories: Array<any>;
  public totalTime = null;
  public booths: [];
  public PASSWORD_NEW: string;
  public PASSWORD_CONFIRM: string;

  public hour;
  public min;

  constructor(
    public router: Router,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
    this.getMyInfo();
    this.getMyHistory();
    this.getVisitBooth();
  }

  /** 사용자 정보 조회 */
  getMyInfo(): void {
    this.memberService.getMyInfo(this.user.id).subscribe(res => {
      // console.log('GET MyInfo', res);
      if (res) {
        this.user = res;
      }
    });
  }

  /** 사용자 시청기록 조회 */
  getMyHistory(): void {
    this.memberService.getMyHistory(this.user.id).subscribe(res => {
      if (res && res.logs) {
        this.histories = res.logs;
        // this.totalTime = res.totalTime;

        this.hour = Math.floor(res.totalTime / 60);
        this.min = res.totalTime % 60;

        // this.totalTime = new Date(0, 0, 0, 0, res.totalTime);
      }
    });
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      localStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      localStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id
    });
  }

  /** 비밀번호 변경 */
  changePassword(): void {
    const errorMessage = this.passwordValidator();
    if (!errorMessage) {
      this.memberService.changePassword(this.user.id, this.PASSWORD_CONFIRM).subscribe(res => {
        alert('비밀번호가 변경되었습니다.');
        this.cancelChangePwd();
      });
    } else {
      alert(errorMessage);
    }
  }

  /** 비밀번호 변경 취소 */
  cancelChangePwd(): void {
    $('#changePassword').modal('hide');
    this.PASSWORD_NEW = '';
    this.PASSWORD_CONFIRM = '';
  }

  /** 비밀번호 변경 유효성 검사 */
  passwordValidator(): string {
    if (!this.PASSWORD_NEW || !this.PASSWORD_CONFIRM) {
      return Change_Password_Message.FILL_ALL;
    } else if (this.PASSWORD_NEW !== this.PASSWORD_CONFIRM) {
      return Change_Password_Message.FAILED_NEW_CONFIRM;
    }
    return null;
  }

  getVisitBooth() {
    this.memberService.findVisitors(this.user.id)
      .subscribe((data: any) => {
        this.booths = data;
      });
  }
}
