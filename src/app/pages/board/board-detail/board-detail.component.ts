import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { BoardService } from 'src/app/services/api/board.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-board-detail',
  templateUrl: './board-detail.component.html',
  styleUrls: ['./board-detail.component.scss']
})
export class BoardDetailComponent implements OnInit {

  public board: any;
  public user: any;

  public formGroup: FormGroup;
  public isEditMode: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private boardService: BoardService,
    private fb: FormBuilder
  ) {
    if (localStorage.getItem('cfair')) {
      this.user = JSON.parse(localStorage.getItem('cfair'))
    }
    this.formGroup = this.fb.group({
      password: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9.-_-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')])],
    })

    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((val: any) => {
        if (val.url) {
          let urls = val.url.split('/');
          if (urls[1] === 'board' && urls[2]) {
            this.getBoardById(urls[urls.length - 1]);
          }
        }
      });
  }

  ngOnInit(): void {
    let boardId = this.route.snapshot.params.boardId;
    this.getBoardById(boardId);
  }

  getBoardById(boardId: string) {
    this.boardService.findOne(boardId)
      .subscribe((resp: any) => {
        this.board = resp;
      });
  }

  // 이메일 / 비번 확인
  submit() {
    this.isEditMode = false;
    if ((this.formGroup.value.email !== this.board.email) || (this.formGroup.value.password.toString() !== this.board.password)) {
      return alert('글 작성시 입력한 이메일 또는 비밀번호를 확인해주세요.');
    } else {
      this.isEditMode = true;
    }
  }

  // 게시글 삭제하기
  delete() {
    let result = confirm('이 게시글을 정말로 삭제하시겠습니까?');
    if (result) {
      this.boardService.delete(this.board.id).subscribe((resp: any) => {
        // console.log(resp);
        alert('삭제되었습니다.');
        this.router.navigate(['/board'], { replaceUrl: true })
      })
    }

  }

}
