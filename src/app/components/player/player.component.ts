import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import { MemberService } from 'src/app/services/api/member.service';
import videojs from 'video.js';
import * as _ from 'lodash';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { SocketService } from 'src/app/services/socket/socket.service';
import { Subscription } from 'rxjs';
import { BannerService } from 'src/app/services/api/banner.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('videoPlayer') videoPlayer: ElementRef;
  @Input('content') content: any; // 전달받은 콘텐츠 정보
  @Input('isLive') isLive = false; // 댓글 유무 확인 (상세에서는 채팅이 없기때문에 false로 받는다.);
  @Input('selected') selected: any; // 전달받은 날짜/룸(채널) 정보
  @ViewChild('commentList') commentList: any; // 댓글 목록

  // @Input('isVod') isVod = false;
  public banner;

  public user: any;
  public videoPlayerObject;
  public reg = /vimeo.com/;
  public player: any;

  public liveUrl: SafeResourceUrl;

  // TODO 임시데이타
  public activeSponsor: any = '1';
  public replyForm: FormGroup;
  public replys: any = [];

  private timerID;

  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
    public sanitizer: DomSanitizer,
    private bannerService: BannerService,
  ) {
    this.replyForm = fb.group({
      content: ['', Validators.compose([Validators.required, Validators.minLength(10)])]
    });

  }

  ngOnDestroy(): void {
    clearInterval(this.timerID);
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
    this.cleanUrl(this.content.contentUrl);
    // this.getReplys();

    if (this.isLive) {
      this.getComment();

      this.timerID = setInterval(() => {
        this.getComment();
      }, 10 * 1000);

      // this.getBanner();
    }
  }


  /** 채팅 상단 배너 랜덤 출력 */
  // getBanner(): void {
  //   this.bannerService.find().subscribe(res => {
  //     if (res.live) {
  //       const items = [];
  //       for (const live of res.live) {
  //         for (let i = 0; i < live.priority; i++) {
  //           items.push(live); // 가지고 있는 비율만큼 배열에 들어간다.
  //         }
  //       }

  //       const randomNumber = Math.floor(Math.random() * (items.length));
  //       this.banner = items[randomNumber];
  //     }
  //   });
  // }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
    // if (this.isVod) {
    //   this.setVideoPlayer();
    // }

  }

  /** 비메오 url trust */
  cleanUrl(url): void {
    this.liveUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  /** 비디오 플레이어 셋팅 */
  setVideoPlayer(): void {
    if (this.reg.test(this.content.contentUrl)) {  // Vimeo
      const opt: any = {
        controls: false,
        controlBar: {
          subsCapsButton: false,
          subtitlesButton: false,
          pictureInPictureToggle: false,
        },
        fluid: true,
        html5: { nativeTextTracks: false },
        preload: 'auto',
        thumbnail: this.content.thumbNailUrl,
        sources: {
          src: this.content.contentUrl,
          type: 'video/vimeo'
        },
      };

      this.player = new videojs(this.videoPlayer.nativeElement, opt, () => {
        const that = this;
        this.player.on('error', (error) => {
          // console.log('error');
          this.offVideoEvent();
        });

        // 동영상 재생 위치 변경 감지
        this.player.on('loadeddata', () => {
          // console.log('loadeddata');
          that.player.off('loadeddata');
        });

        // 플레이어 초기 지속 시간 발생 감지
        this.player.on('loadedmetadata', () => {
          // console.log('loadedmetadata');
          that.player.off('loadedmetadata');
        });

        // 동영상 종료
        this.player.on('ended', () => {
          // console.log('ended');
          that.player.off('ended');
          that.player.initChildren();
        });

        // 동영상 재생
        this.player.on('play', () => {
          // console.log('play');
        });

        // 동영상 일시정지
        // 기존에 일시정지하면 바로 서버에 로그를 기록했는데, 클라이언트에서 10초 간격으로 계산하여 서버에 요청하는 구조로 바뀜.
        // 이력은 그대로 냅둔다.
        this.player.on('pause', () => {
          // console.log('pause');
        });
      });
    }
  }

  // 비디오 이벤트 제거
  offVideoEvent = () => {
    if (this.player) {
      this.player.off('loaded');
      this.player.off('ended');
      this.player.off('play');
      this.player.off('pause');
      this.player.off('seeked');
      this.player.off('seekable');
      // this.player.off('timeupdate');
    }
  }

  /** 댓글 불러오기 */
  getComment(): void {
    this.memberService.findComment(this.user.id, this.selected.room.id).subscribe(res => {
      this.replys = res;
    });
  }

  /** 댓글달기 */
  comment(): void {
    this.memberService.createComment({
      memberId: this.user.id,
      title: '',
      content: this.replyForm.value.content,
      relationId: this.selected.room.id
    }).subscribe(res => {
      this.getComment();
      // this.replyForm.value.content = '';
      this.replyForm.patchValue({
        content: ''
      });

      // this.downScroll();
    });
  }

  /** 채팅창을 최하단으로 스크롤한다. */
  downScroll(): void {
    this.commentList.nativeElement.scrollTop = this.commentList.nativeElement.scrollHeight;
  }

  /** 본인이 입력한 채팅 여부 (삭제 출력) */
  check(comment): boolean {
    if (comment && comment.memberId === this.user.id) {
      return true;
    }
    return false;
  }

  /** 채팅을 삭제한다. */
  delete(comment): void {
    if (confirm('삭제하시겠습니까?')) {
      this.memberService.deleteComment(this.user.id, comment.id)
        .subscribe(res => {
          this.getComment();
        });
    }
  }
}
