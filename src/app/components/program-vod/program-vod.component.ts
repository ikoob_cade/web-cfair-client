import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-program-vod',
  templateUrl: './program-vod.component.html',
  styleUrls: ['./program-vod.component.scss']
})
export class ProgramVodComponent implements OnInit {
  @Input('vod') vod: any;

  constructor(public router: Router) { }

  ngOnInit(): void { }

  goDetail(): void {
    this.router.navigate([`/vod/${this.vod.id}`]);
  }

}
