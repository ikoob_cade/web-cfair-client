import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { DateService } from 'src/app/services/api/date.service';
import { RoomService } from 'src/app/services/api/room.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.scss']
})
export class TimeTableComponent implements OnInit, AfterContentChecked {

  @ViewChild('table1', { read: ElementRef }) table1: ElementRef;
  @ViewChild('table2', { read: ElementRef }) table2: ElementRef;
  @ViewChild('th1', { read: ElementRef }) th1: ElementRef;
  @ViewChild('th2', { read: ElementRef }) th2: ElementRef;
  // tslint:disable-next-line: no-input-rename
  @Input('agendas') agendas: any;


  public channelBtnStyle1: any;
  public channelBtnStyle2: any;
  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef,
    private dateService: DateService,
    private roomService: RoomService,
  ) { }

  ngOnInit(): void {
    this.doInit();
  }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
    this.setChannelBtnStyle();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.setChannelBtnStyle();
  }

  setChannelBtnStyle(): void {
    this.channelBtnStyle1 = {
      height: this.table1.nativeElement.clientHeight - this.th1.nativeElement.clientHeight + 'px',
      top: this.th1.nativeElement.clientHeight + 'px'
    }
    this.channelBtnStyle2 = {
      height: this.table2.nativeElement.clientHeight - this.th2.nativeElement.clientHeight + 'px',
      top: this.th2.nativeElement.clientHeight + 'px'
    }
  }

  dates: any;
  rooms: any;

  doInit(): void {
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .subscribe(resp => {
        this.dates = resp[0];
        this.rooms = resp[1];
      });
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * Live 상세보기
   * @param date 순서
   * @param room 순서
   */
  goLive(date: number, room: number): void {
    this.router.navigate(['/live'], { queryParams: { dateId: this.dates[date].id, roomId: this.rooms[room].id } });
  }

}
