import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  // in, out
  private attendance: string;

  constructor(
  ) { }

  public isAttended() {
    return this.attendance ? this.attendance === 'in' ? true : false : true;
  }

  public setAttendance(attendance) {
    this.attendance = attendance;
  }

  public getAttendance() {
    return this.attendance;
  }

}
